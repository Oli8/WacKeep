<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/fontello.css">
    <title>WacKeep</title>
</head>
<body>
	<header>  
        <h1><a href="#">WacKeep</a></h1>
        <div class="navright hide">
            <span id="user_pseudo"></span><br/>
            <a href="#" id="logout"><i class="icon-logout"></i>Log out</a>
        </div>
    </header>

    <main>
        <section id="content">
            <div id="notifs" class="hide"></div>

            <form id="note">
                <h3>New note</h3>
            	<input type="text" class="title" autofocus placeholder="Title">
                <textarea class="content">Content</textarea><br/>
                <label for="color">Color : </label><input type="color" id="color" value="#fefabc"></input><br/>
                Share with :
                <select id="share">
                    <option>Private</option>
                </select> <button id="shareSubmit">Ok</button>

                <div id="sharedWith"></div>
        		<input type="submit"/>
        	</form>

            <form id="editNote" class="hide">
                <h3>Edit note</h3>
                <input type="hidden" class="idNote">
                <input type="text" class="title" autofocus placeholder="Title">
                <textarea class="content">Content</textarea><br/>
                <label for="color">Color : </label><input type="color" id="color" value="#fefabc"></input>
                <input type="submit"/>
            </form>
        
            <form id="signin">
                <h3>Login</h3>
                <input type="text" placeholder="Pseudo" class="login"/>
                <input type="password" placeholder="password" class="pass"></input>
                <input type="submit">
            </form>    

            <form id="signup">
                <h3>Sign up</h3>
                <input type="text" placeholder="Pseudo" class="login"/>
                <input type="password" placeholder="password" class="pass"></input>
                <input type="submit">
            </form>

            <div id="notes"></div>

            <div id="sharedNotes" class="hide">
                <h3>Shared with you : </h3>
                <div class="wrap"></div>
            </div>
        </section>
    </main>

    <footer>
        <section> © 2016 | Olivier Crété </section>
    </footer>

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>
-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 27 Septembre 2016 à 22:52
-- Version du serveur :  5.6.31-0ubuntu0.15.10.1
-- Version de PHP :  5.6.11-1ubuntu3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `WacKeep`
--

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT 'My note',
  `content` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `notes`
--

INSERT INTO `notes` (`id`, `id_user`, `title`, `content`, `color`) VALUES
(5, 1, 'newtest', 'Content', ''),
(9, 1, 'plapla', 'Content', '#9a3d1e'),
(10, 1, 'plapla', 'Content', '#ae6e58'),
(11, 1, 'plapla', 'Content', '#4c61c1'),
(12, 1, 'new', 'newContent', '#389a25'),
(78, 1, 'test share', 'Content', '#bcfeca'),
(79, 1, 'My note', 'Content', '#fefabc'),
(80, 1, 'lol', 'Content', '#fefabc'),
(81, 1, 'test share db', 'qzmdokqzmdok', '#fee0bc'),
(82, 2, 'testos', 'Content', '#55543e'),
(83, 2, 'My note', 'yolo', '#55543e'),
(85, 4, 'shared note', 'my first shared note D:', '#b6b5a5'),
(86, 4, 'shared note 2', 'second shared note', '#b6b5a5'),
(91, 1, 'test share', 'sharetest', '#bcc2fe'),
(92, 4, 'new share test', 'is it working?', '#a19919'),
(93, 2, 'share with oli', 'Content', '#9a943a'),
(94, 1, 'My note', 'Content', '#fefabc'),
(95, 4, 'hm', 'last node shared', '#fefabc'),
(96, 4, 'black note :/', 'can you see me?', '#050500'),
(97, 2, 'My note', 'Content\nplop', '#fefabc'),
(98, 2, 'qzdqzd', 'Content', '#fefabc'),
(99, 2, 'qzd', 'qzmodk\nqzdqzd\nqzdqzd', '#fefabc'),
(100, 2, 'My note', 'Content', '#fefabc'),
(102, 2, 'test notifs', 'blablah', '#d922bd'),
(103, 2, 'sef', 'sefsefseff', '#d922bd'),
(104, 2, 'My note', '', '#d922bd'),
(107, 2, 'notif test', 'plapal', '#fefabc'),
(108, 2, 'test notifs ', 'Content notifs', '#c0ba52'),
(109, 1, 'test notif', 'Content', '#fefabc'),
(110, 1, 'retest notif ', 'plapla', '#fefabc'),
(111, 1, 'test notifs', 'Content', '#fefabc');

-- --------------------------------------------------------

--
-- Structure de la table `notifs`
--

CREATE TABLE IF NOT EXISTS `notifs` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `notifs`
--

INSERT INTO `notifs` (`id`, `id_user`, `text`) VALUES
(9, 3, 'olivier.crete@orange.com has shared a note with you.');

-- --------------------------------------------------------

--
-- Structure de la table `share`
--

CREATE TABLE IF NOT EXISTS `share` (
  `id_note` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `share`
--

INSERT INTO `share` (`id_note`, `id_user`) VALUES
(91, 4),
(92, 1),
(92, 2),
(93, 1),
(95, 1),
(95, 2),
(101, 1),
(101, 3),
(102, 3),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(105, 4),
(107, 1),
(107, 3),
(108, 1),
(108, 3),
(110, 4),
(111, 4);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `login`, `password`) VALUES
(1, 'Oli8', 'password'),
(2, 'olivier.crete@orange.com', 'pass'),
(3, 'Loic', 'Toit'),
(4, 'jrm', 'pass');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `notifs`
--
ALTER TABLE `notifs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT pour la table `notifs`
--
ALTER TABLE `notifs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

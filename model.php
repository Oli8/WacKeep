<?php

class Model{

	public $user = 'root';
	public $pass = 'root';
	public $dbname = 'WacKeep';
	public $db;

	public function __construct(){
		$this->db = new PDO("mysql:host=localhost;dbname=$this->dbname", $this->user, $this->pass);
	}

	public function getId($login){
		return $this->db->query("SELECT id FROM users WHERE login = '$login'")->fetchAll(PDO::FETCH_OBJ)[0]->id;
	}

	public function signin(){
		extract($_POST);

		echo count($this->db->query("SELECT * FROM users WHERE login = '$pseudo' AND password = '$pass'")->fetchAll(PDO::FETCH_OBJ)) > 0 ? "success" : "fail";
	}

	public function signup(){
		$login = $_POST['pseudo'];
		$pass = $_POST['pass'];

		if(isset($login) && isset($pass)){
			$this->db->exec("INSERT into users VALUES ('', '$login', '$pass')");
			echo "success";
		}
		else echo "fail";
	}

	public function deleteNote(){
		$id = $_POST['id'];

		$this->db->exec("DELETE FROM notes WHERE id = '$id'");
		echo "Note deleted.";
	}

	public function editNote(){
		extract($_POST);

		$this->db->exec("UPDATE notes SET title = '$title', content = '$content', color = '$color' WHERE id = '$id'");
		echo "Note updated";
	}

	public function getNote(){
		$id = $_POST['id'];

		echo json_encode($this->db->query("SELECT * FROM notes WHERE id = '$id'")->fetchAll(PDO::FETCH_OBJ));
	}

	public function getNotes(){
		$login = $_POST['pseudo'];
		$id = $this->getId($login);

		echo json_encode($this->db->query("SELECT * FROM notes WHERE id_user = '$id'")->fetchAll(PDO::FETCH_OBJ));
	}

	public function getSharedNotes(){
		$id = $this->getId($_POST['pseudo']);

		echo json_encode($this->db->query("SELECT * FROM notes WHERE id in (SELECT id_note FROM share where id_user = '$id')")->fetchAll(PDO::FETCH_OBJ));
	}

	public function getUsers(){
		$login = $_POST['pseudo'];
		$id = $this->getId($login);

		echo json_encode($this->db->query("SELECT * FROM users WHERE id != '$id'")->fetchAll(PDO::FETCH_OBJ));
	}

	public function note(){
		extract($_POST);
		$id = $this->getId($pseudo);

		$this->db->exec("INSERT INTO notes VALUES ('', '$id', '$title', '$content', '$color')");

		echo "Note saved.";
	}

	public function shareNote(){
		$users = array_map('intval', explode(' ', $_POST['users']));
		$lastId = $this->db->query("SELECT max(id) as id FROM notes")->fetchAll(PDO::FETCH_OBJ)[0]->id;

		foreach($users as $user)
			$this->db->exec("INSERT INTO share VALUES ('$lastId', '$user')");
	}

	public function addNotif(){
		$ids = array_map('intval' , explode(' ', $_POST['ids']));
		$pseudo = $_POST['pseudo'];
		$msg = "$pseudo has shared a note with you.";

		foreach($ids as $id)
			$this->db->exec("INSERT INTO notifs VALUES ('', '$id', '$msg')");
	}

	public function getNotifs(){
		$id = $this->getId($_POST['pseudo']);

		echo json_encode($this->db->query("SELECT * FROM notifs WHERE id_user = '$id'")->fetchAll(PDO::FETCH_OBJ));
	}

	public function deleteNotif(){
		$id = $_POST['id'];
		$this->db->exec("DELETE FROM notifs WHERE id = '$id'");
	}
}

(New Model())->$_GET['method']();
?>
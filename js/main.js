if(localStorage.getItem('WacKeep')){
	connect();
	getNotes();
	getSharedNotes();
	getUsers();
	getNotifs();
}

window.setInterval(function(){
	if(localStorage.getItem('WacKeep'))
		getNotifs();
}, 5000);

$('#signup').on('submit', function(e){
	e.preventDefault();
	$.post('model.php?method=signup', {pseudo: $('#signup .login').val(), pass: $('#signup .pass').val()}, function(data){
		alert(data == 'success' ? 'Signup successful' : 'Signup has failed');
	});	
});

$('#signin').on('submit', function(e){
	e.preventDefault();
	var login = $('#signin .login').val();
	$.post('model.php?method=signin', {pseudo: login, pass: $('#signin .pass').val()}, function(data){
		var success = (data == 'success');
		if(!success)
			return alert('Signin has failed');
		else{
			localStorage.setItem('WacKeep', login);
			connect();
		}
		getNotes();
		getSharedNotes();
		getUsers();
		getNotifs();
	});
});

$('#note').on('submit', function(e){
	e.preventDefault();
	$.post('model.php?method=note', {pseudo: $('#user_pseudo').text(), 
						title: $('#note .title').val() || 'My note', 
						content: $('#note .content').val(),
						color: $('#note #color').val()}, function(data){
	}).done(function(){
		getNotes();
		$('#note .title').val('');
		$('#note .content').val('');
		
		if(sharedWith.length){
			$.post('model.php?method=addNotif', {ids: sharedWith.map(function(v){
					return v.id;
				}).join(' '), pseudo: $('#user_pseudo').text()}, function(data){

			}).done(function(){
				$.post('model.php?method=shareNote', {users: sharedWith.map(function(v){
					return v.id;
				}).join(' ')}, function(data){

				}).done(function(){
					$('#sharedWith').empty();
					sharedWith = [];
				});
			});
		}
	});
});

function getNotes(){
	var notes, angle, content;
	$('#notes').empty();
	$.post('model.php?method=getNotes', {pseudo: $('#user_pseudo').text()}, function(data){
		for(var v of JSON.parse(data)){
			angle = Math.random() * (8 - 1) + 1;
			angle *= Math.random() > 0.5 ? -1 : 1;
			content = nl2br(v.content);
			$('#notes').append(`<div class='post-it' style='transform: rotate(${angle}deg); background-color: ${v.color}'>
									<h1>${v.title}</h1>
									<i onclick='deleteNote(${v.id})' class='icon-trash'></i>
									<i onclick='editNote(${v.id})' class='icon-edit'></i>
									<p>${content}</p>
								</div>`);
		}
	});
}

function connect(){
	$('.navright').show();
	$('#user_pseudo').html("<i class='icon-user'></i>" + localStorage.getItem('WacKeep'));
	$('#signin').hide();
	$('#signup').hide();
	$('#note').show();
}

$('#logout').click(function(){
	localStorage.removeItem('WacKeep');
	location.reload();
});

function deleteNote(id){
	$.post('model.php?method=deleteNote', {id: id}, function(data){
		alert(data);
	}).done(getNotes);
}

function editNote(id){
	var note = {};
	$('#note').hide();
	$('#editNote').show();
	$.post('model.php?method=getNote', {id: id}, function(data){
		var noteData = JSON.parse(data)[0];
		for(v of ['id', 'title', 'content', 'color'])
			note[v] = noteData[v];
	}).done(function(){
		$('#editNote .idNote').val(note.id);
		$('#editNote .title').val(note.title);
		$('#editNote .content').val(note.content);
		$('#editNote #color').val(note.color);
	});
}

$('#editNote').on('submit', function(e){
	e.preventDefault();
	$.post('model.php?method=editNote', {id: $('#editNote .idNote').val(),
							title: $('#editNote .title').val(),
							content: $('#editNote .content').val(),
							color: $('#editNote #color').val()}, function(data){
		alert(data);
	}).done(function(){
		$('#editNote').hide();
		$('#note').show();
		getNotes();
	});
});

function getUsers(){
	while(1){
		if($('#user_pseudo').text())
			return $.post('model.php?method=getUsers', {pseudo: $('#user_pseudo').text()}, function(data){
			var users = JSON.parse(data);
			for(var v of users)
				$('#share').append(`<option value='${v.id} ${v.login}'>${v.login}</option>`);
		});
	}
}

var sharedWith = [];
$('#shareSubmit').click(function(e){
	e.preventDefault();
	var value = $('#share').val().split(" ");
	var id = value[0];
	var nick = value[1];

	if(value != 'Private' && $('#sharedWith').html().indexOf(nick) === -1)
		sharedWith.push({id: id, nick: nick});
	
	$('#sharedWith').html(sharedWith.map(function(v){
		return v.nick;
	}).join(', '));
});

function getSharedNotes(){
	var sharedNotes, angle;
	$('#sharedNotes div.wrap').empty();
	$.post('model.php?method=getSharedNotes', {pseudo: $('#user_pseudo').text()}, function(data){
		sharedNotes = JSON.parse(data);
		if(sharedNotes.length) $('#sharedNotes').show();
	}).done(function(){
		for(var v of sharedNotes){
			angle = Math.random() * (8 - 1) + 1;
			angle *= Math.random() > 0.5 ? -1 : 1;
			$('#sharedNotes div.wrap').append(
				`<div class='post-it' style='transform: rotate(${angle}deg); background-color: ${v.color}'>
					<h1>${v.title}</h1>
					<p>${v.content}</p>
				</div>`);
		}
	});
}

function getNotifs(){
	var notifs;
	$.post('model.php?method=getNotifs', {pseudo: $('#user_pseudo').text()}, function(data){
		notifs = JSON.parse(data);
	}).done(function(){
		if(notifs.length){
			$('#notifs').empty();
			$('#notifs').show();
			for(notif of notifs){
				$('#notifs').append(
					`<div class="notif${notif.id}">
						<i class='icon-cancel-circled' onclick='deleteNotif(${notif.id})'></i>
						<p><i class='icon-info-circled'></i>${notif.text}</p>
					</div>`);
			}
		}
		else $('#notifs').hide();
	});
}

function deleteNotif(id){
	$.post('model.php?method=deleteNotif', {id: id}, function(data){

	}).done(function(){
		$(`.notif${id}`).remove();
		getNotifs();
		getSharedNotes();
	});
}

function nl2br(str, is_xhtml){
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}